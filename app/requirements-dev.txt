black==19.10b0
flake8===3.7.9
isort==4.3.21
pytest-cov==2.8.1
pytest-django==3.6.0
pytest==5.2.2

-r requirements.txt
